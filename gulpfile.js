'use strict';
 
var scssDir = './wp-content/themes/bankers/sass',
    cssDir = './wp-content/themes/bankers/css';

var gulp            = require('gulp');
var sass            = require('gulp-sass');
var postcss         = require('gulp-postcss');
var sourcemaps      = require('gulp-sourcemaps');
var autoprefixer    = require('autoprefixer');
var rename          = require('gulp-rename');
 
sass.compiler = require('node-sass');
 
gulp.task('sass', function () {
  return gulp.src(scssDir + '/import.scss')
    .pipe(sourcemaps.init())
    .pipe(sass({outputStyle:'compressed'}).on('error', sass.logError))
    .pipe(postcss([ autoprefixer() ]))
    .pipe(rename('style.css'))
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest(cssDir));
});
 
gulp.task('sass:watch', function () {
  gulp.watch(scssDir + '/**/*.scss', gulp.series('sass'));
});