<?php 
add_action( 'init', 'components_cpt' );
/**
 * Register a Busness Unit post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function components_cpt() {
	$labels = array(
		'name'               => _x( 'Components', 'post type general name' ),
		'singular_name'      => _x( 'Component', 'post type singular name' ),
		'menu_name'          => _x( 'Components', 'admin menu' ),
		'name_admin_bar'     => _x( 'Component', 'add new on admin bar' ),
		'add_new'            => _x( 'Add New', 'Component' ),
		'add_new_item'       => __( 'Add New Component' ),
		'new_item'           => __( 'New Component' ),
		'edit_item'          => __( 'Edit Component' ),
		'view_item'          => __( 'View Component' ),
		'all_items'          => __( 'All Components' ),
		'search_items'       => __( 'Search Components' ),
		'parent_item_colon'  => __( 'Parent Components:' ),
		'not_found'          => __( 'No Components found.' ),
		'not_found_in_trash' => __( 'No Components found in Trash.' )
	);

	$args = array(
		'labels'             => $labels,
		'description'        => __( 'Description.' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
        'show_in_menu'       => true,
        'show_in_rest'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'Component' ),
		'capability_type'    => 'post',
		'has_archive'        => false,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title' )
	);

    register_post_type( 'components', $args );
}