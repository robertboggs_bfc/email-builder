<?php 
add_action( 'init', 'modules_cpt' );
/**
 * Register a Busness Unit post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function modules_cpt() {
	$labels = array(
		'name'               => _x( 'Modules', 'post type general name' ),
		'singular_name'      => _x( 'Module', 'post type singular name' ),
		'menu_name'          => _x( 'Modules', 'admin menu' ),
		'name_admin_bar'     => _x( 'Module', 'add new on admin bar' ),
		'add_new'            => _x( 'Add New', 'Module' ),
		'add_new_item'       => __( 'Add New Module' ),
		'new_item'           => __( 'New Module' ),
		'edit_item'          => __( 'Edit Module' ),
		'view_item'          => __( 'View Module' ),
		'all_items'          => __( 'All Modules' ),
		'search_items'       => __( 'Search Modules' ),
		'parent_item_colon'  => __( 'Parent Modules:' ),
		'not_found'          => __( 'No Modules found.' ),
		'not_found_in_trash' => __( 'No Modules found in Trash.' )
	);

	$args = array(
		'labels'             => $labels,
		'description'        => __( 'Description.' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
        'show_in_menu'       => true,
        'show_in_rest'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'Module' ),
		'capability_type'    => 'post',
		'has_archive'        => false,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title' )
	);

  register_post_type( 'modules', $args );
}

