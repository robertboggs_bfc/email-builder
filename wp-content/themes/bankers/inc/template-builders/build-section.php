<?php 
function build_section($contents, $section_classes) {
    $headline = get_sub_field('headline');
    $section_id = '';
    $section_label = '';
    if($headline['copy']) {
        $section_label = $headline['copy'];
        $section_id = slugify($headline['copy']);
    }
    ob_start();?>
        <section class="<?php echo $section_classes;?>" id="<?php echo $section_id; ?>" aria-label="<?php echo $section_label;?>">
            <?php echo $contents?>
        </section>
    <?php 
    echo ob_get_clean();
}