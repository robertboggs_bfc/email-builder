<?php 
add_action( 'init', 'business_unit_tax' );
/**
 * Register a Busness Unit post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function business_unit_tax() {
// Add new taxonomy, NOT hierarchical (like tags)
	$labels = array(
		'name'                       => _x( 'Business Units', 'taxonomy general name', 'textdomain' ),
		'singular_name'              => _x( 'Business Unit', 'taxonomy singular name', 'textdomain' ),
		'search_items'               => __( 'Search Business Units', 'textdomain' ),
		'popular_items'              => __( 'Popular Business Units', 'textdomain' ),
		'all_items'                  => __( 'All Business Units', 'textdomain' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Business Unit', 'textdomain' ),
		'update_item'                => __( 'Update Business Unit', 'textdomain' ),
		'add_new_item'               => __( 'Add New Business Unit', 'textdomain' ),
		'new_item_name'              => __( 'New Writer Name', 'textdomain' ),
		'separate_items_with_commas' => __( 'Separate categories with commas', 'textdomain' ),
		'add_or_remove_items'        => __( 'Add or remove categories', 'textdomain' ),
		'choose_from_most_used'      => __( 'Choose from the most used categories', 'textdomain' ),
		'not_found'                  => __( 'No categoriess found.', 'textdomain' ),
		'menu_name'                  => __( 'Business Units', 'textdomain' ),
	);

	$args = array(
		'hierarchical'          => false,
		'labels'                => $labels,
		'show_ui'               => true,
        'show_admin_column'     => true,
        'show_in_rest'          => true,
        'rest_base'             => 'business-unit',
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'business-unit' ),
	);

	register_taxonomy( 'business-unit', ['modules', 'components'], $args );
}
