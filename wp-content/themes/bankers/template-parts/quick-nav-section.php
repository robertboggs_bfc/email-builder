<?php
    $headline = get_sub_field('headline');
    $section_classes = 'quick-nav-section';
    $headline = get_sub_field('headline');
    $section_id = '';
    $section_label = '';
    if($headline['copy']) {
        $section_label = $headline['copy'];
        $section_id = slugify($headline['copy']);
    }
?>
<nav 
    id="<?php echo $section_id;?>" 
    aria-label="<?php echo $section_label;?>" 
    class="<?php echo $section_classes;?>"
>
    <div class="container">
        <?php if($headline['show_headline'] && $headline['copy']):?>
            <h2><?php echo $headline['copy'];?></h2>
        <?php endif;?>

        <?php if(have_rows('navigation_items')):?>
            <ul class="quick-nav-section--nav-items">
                <?php while(have_rows('navigation_items')): the_row();
                    $icon_cond = get_sub_field('show_icon');
                    $icon = get_sub_field('icon');
                    $url = get_sub_field('cta');
                ?>
                    <li class="nav-item">
                        <a class="nav-item--url" href="<?php echo $url['url'];?>">
                            <?php if($icon_cond):?>
                                <div class="nav-item--icon">
                                    <?php if($icon['icon_type'] == 'fa'):?>
                                        <span class="fa-icon <?php echo $icon['font_awesome_icon'];?>"></span>
                                    <?php elseif($icon['icon_type'] == 'image'):?>
                                        <img alt="" class="nav-item--icon__default" src="<?php echo $icon['icon_default']['url'];?>">
                                        <img alt="" class="nav-item--icon__hover" src="<?php echo $icon['icon_hovered']['url'];?>">
                                    <?php endif;?>
                                </div>
                            <?php endif;?>
                            <span class="nav-item--text">
                                <?php echo $url['title'];?>
                            </span>
                        </a>
                    </li>
                <?php endwhile;?>
            </ul>
        <?php endif;?>
    </div>
</nav>