<?php if(have_rows('flexible_layout')):?>
    <?php while(have_rows('flexible_layout')): the_row();?>
        <?php if(get_row_layout() == 'hero_simple'):?>
            <?php get_template_part('template-parts/hero-simple');?>
        <?php endif;?>
        <?php if(get_row_layout() == 'one_column'):?>
            <?php get_template_part('template-parts/one-column');?>
        <?php endif;?>
        <?php if(get_row_layout() == 'two_column'):?>
            <?php get_template_part('template-parts/two-column');?>
        <?php endif;?>
        <?php if(get_row_layout() == 'three_column'):?>
            <?php get_template_part('template-parts/three-column');?>
        <?php endif;?>
        <?php if(get_row_layout() == 'secondary_nav'):?>
            <?php get_template_part('template-parts/secondary-nav');?>
        <?php endif;?>
        <?php if(get_row_layout() == 'quick_navigation'):?>
            <?php get_template_part('template-parts/quick-nav-section');?>
        <?php endif;?>
        <?php if(get_row_layout() == 'form_section'):?>
            <?php get_template_part('template-parts/form-section');?>
        <?php endif;?>
        <?php if(get_row_layout() == 'divider'):?>
            <?php get_template_part('template-parts/divider');?>
        <?php endif;?>
        <?php if(get_row_layout() == 'divider_bold'):?>
            <?php get_template_part('template-parts/divider-bold');?>
        <?php endif;?>
    <?php endwhile;?>
<?php endif;?>