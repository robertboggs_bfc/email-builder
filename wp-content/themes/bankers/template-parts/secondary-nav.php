<?php
    $headline = get_sub_field('headline');
    $section_classes = 'secondary-nav-section';
    $label = slugify($headline['copy']);
    $menu = get_sub_field('menu_select');
?>
<nav 
    id="<?php echo $label;?>" 
    aria-label="<?php echo $label;?>" 
    class="<?php echo $section_classes;?>"
>
    <div class="container">
        <?php if($headline['show_headline'] && $headline['copy']):?>
            <h2><?php echo $headline['copy'];?></h2>
        <?php endif;?>

        <?php
        wp_nav_menu( array(
            'menu' => $menu,
            'menu_id'        => 'secondary-menu',
        ) );
        ?>
    </div>
</nav>